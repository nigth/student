# 06. Define a function `caesarCipher` that takes string and `key(number)`, whuch returns encrypted string

def caesarCipher (arg, key):
    l = len(arg)
    key = int (key)
    result = ''
    for n in range(0, l) :
        new = ord(arg[n]) + key
        if  ord(arg[n]) < 32 or ord(arg[n]) > 126:
            result +=  chr(ord(arg[n]))
        else:
            if new <= 126:
                result += chr(new)
            else:
                result += chr(new%126 + 31)
       
    print ('Using key =',key, ', the encrypted string is: \n',result)

print ('\nThe program works with next 95 letters and symbols, including space): ')
for c in range (32,127):
    print (chr(c), end='', flush=True)
print ('\n')
phrase = input('Please input some string to cipher it by Caesar:\n').strip()
keynum = input('Please input integer key number for Caesar cipher (by default = 1): ')
#if not keynum.isdigit():
if not keynum.isnumeric():    
   keynum = 1

caesarCipher (phrase, keynum)


