# 08. Write a function `game()` number-guessing game, that takes 2 int parameters defining the range.
# Using some kind of `random` function to generate random int from the range. While user input 
# isn't equal that number, print "Try again!". If user guess the number, congratulate him and exit.

import random

def game(x, y):
    generated = random.randint(x,y)
    print ("\nPlease guess my generated integer number from range [",x,';',y,']:')
    guess = int(input())
    while guess != generated:
        guess = int(input('Try again! \n'))
    print ('Yes, it is',generated, ', you win!!!')

a = int(input ("Please input one limit of range: "))
b = int(input ("Please input other limit of range: "))
if a > b:
    a, b = b, a
if a != b:
    game(a, b)









