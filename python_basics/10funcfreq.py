# 10. Write a function `charFreq()` that takes a string and builds a frequency listing
# of the characters contained in it. Represent the frequency listing as a Python dictionary.
# Try it with something like `charFreq("abbabcbdbabdbdbabababcbcbab")`.

def charFreq (arg):
    freqdic = {arg[0]:0}
    for s in arg:
        if s in freqdic:
            freqdic[s] += 1
        else:
            freqdic[s] = 1

    return freqdic

phrase = input('\nPlease input some string:\n')
result = charFreq (phrase)
for s in result:
    print (s, result[s])
