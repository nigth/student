# 09.  Define a function, which takes a string with N opening brackets `("[") and N closing 
# brackets ("]")`, in some arbitrary order. Determine whether the generated string is balanced; 
# that is, whether it consists entirely of pairs of opening/closing brackets (in that order), 
# none of which mis-nest.    Examples:
#   []        OK   ][        NOT OK
#   [][]      OK   ][][      NOT OK
#   [[][]]    OK   []][[]    NOT OK

def brackets (arg):
    i = 0
    while len(arg) > 2 and i < len(arg)-1:
        if arg[i] == '[' and arg[i+1] == ']':
            arg = arg[:i] + arg[i+2:]
            i = 0
        else:
            i += 1
    if arg[0] == '[' and arg[1] == ']':
        return ('OK')
    else:
        return ('NOT OK') 

print ('Self testing, please wait:')
print ('Expected `OK`', brackets ('[]'))       
print ('Expected `OK`', brackets ('[][]'))
print ('Expected `OK`', brackets ('[[][]]'))    
print ('Expected `NOT OK`', brackets (']['))
print ('Expected `NOT OK`', brackets ('][]['))
print ('Expected `NOT OK`', brackets ('[]][[]'))
print ('Expected `OK`', brackets ('[[][[]]]'))
print ('Expected `NOT OK`', brackets (']'))
print ('Expected `NOT OK`', brackets ('[[['))

phrase = input('Please input N opening brackets [ and N closing brackets ] without spaces:\n').split()
print (brackets (*phrase))