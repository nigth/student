#11. Write a function `decToBin()` that taces decimal integer and outputs its binary representation.

def decToBin (arg):
    binary = ''
    count = 0
    if arg == 0:
        binary = '0'
    else:
        while arg > 0:
            if count%4 == 0:
                binary = ' ' + binary
                count = 0
            binary = str(arg%2) + binary
            arg //= 2
            count += 1
    return binary

print('\nSelf testing, please wait:')
for i in range (17):
    print (i,decToBin (i))

number = int(input('\nPlease input decimal integer: '))
result = decToBin (number)
print ('Its binary representation is:', result)
